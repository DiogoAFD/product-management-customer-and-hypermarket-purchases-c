#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "avl_modulos.h"



char* strdup(char*);

struct MAVL{
    char* id; 
    struct MAVL *left;
    struct MAVL *right;
    int height;
}MAVL;


int height(GAVL N){
	if (N == NULL) return 0;
	return N-> height;
}


int max(int a, int b){
	return (a > b) ? a : b;
}


GAVL newNode (char * id){
    GAVL node = (GAVL)malloc(sizeof(MAVL));
    node->id=strdup(id);
    node->left=NULL;
    node->right=NULL;
    node->height=1;
    return node;
}


GAVL rightRotate(GAVL y){

    GAVL x = y->left;
    GAVL z = x->right;
 
    x->right = y;
    y->left = z;
 
    y->height = max(height(y->left), height(y->right))+1;
    x->height = max(height(x->left), height(x->right))+1;
 
    return x;
}
 

GAVL leftRotate(GAVL x){

    GAVL y = x->right;
    GAVL T2 = y->left;
 
    y->left = x;
    x->right = T2;
 
    x->height = max(height(x->left), height(x->right))+1;
    y->height = max(height(y->left), height(y->right))+1;

    return y;
}


int getBalance(GAVL N){
    if (N == NULL)
        return 0;
    return height(N->left) - height(N->right);
}


int procura(GAVL node, char* id){
    if(node == NULL) return 0;
    if (strcmp(id,node->id) == 0) return 1;
    else if (strcmp(id,node->id) < 0) return procura(node->left, id);
    else return procura(node->right, id);
    
}


GAVL insert(GAVL node, char* id){
    int balance = 0;
    if (node == NULL)
        return(newNode(id));
    if (strcmp(id,node->id) != 0){
        if (strcmp(id,node->id) < 0) node->left  = insert(node->left, id);
        else node->right = insert(node->right, id);
        node->height = max(height(node->left), height(node->right)) + 1;
        balance = getBalance(node);
        if (balance > 1 && strcmp(id,node->left->id) < 0) return rightRotate(node);
        if (balance < -1 &&  strcmp(id,node->right->id) >= 0) return leftRotate(node);
        if (balance > 1 &&  strcmp(id,node->left->id) >= 0){
            node->left =  leftRotate(node->left);
            return rightRotate(node);
        }
        if (balance < -1 &&  strcmp(id,node->right->id) < 0){
            node->right = rightRotate(node->right);
            return leftRotate(node);
        }
        return node;
    }
    else {return node;}
}

/*Querie 2,6 ########################################################################################*/

int getAllElements(GAVL node,char** array,int size){
    if(node!=NULL){
        size=getAllElements(node->left,array,size);
        (array[size])=strdup(node->id);
        size++;
        size=getAllElements(node->right,array,size);
        return size;
        }
        return size;
    }


char** getElements(GAVL node,int totElements,int* size){
    char** array=NULL;
    int aux=0;
    if(node!=NULL){
        array=(char**) malloc(totElements*sizeof(char*));
        array[0]=NULL;
        aux=getAllElements(node,array,0);
    }
    *size=aux;
    return array;
}

/*################################################################################################################*/