#ifndef AVL
#define AVL


typedef struct MAVL* GAVL;

int height(GAVL N);
int max(int a, int b);
GAVL newNode (char * id);
GAVL rightRotate(GAVL y);
GAVL leftRotate(GAVL x);
int getBalance(GAVL N);
int procura(GAVL node, char* id);
GAVL insert(GAVL node, char* id);
int getAllElements(GAVL node,char** array,int size);
char** getElements(GAVL node,int totElements,int* size);


#endif