#ifndef AVL_K
#define AVL_K

typedef struct CAVL* KAVL;
typedef struct BAVL* PAVL;


PAVL init_ProdutList (char* idProd,int mes,int quantidade,char tipo);
int height_avlCompras(KAVL N);
int max_avlCompras(int a, int b);
KAVL newNode_avlCompras (char* idCLien, char* idProd);
KAVL rightRotate_avlCompras(KAVL y);
KAVL leftRotate_avlCompras(KAVL x);
int getBalance_avlCompras(KAVL N);
int procura_avlCompras(KAVL node, char* id);
KAVL insert_avlCompras(KAVL node, char* idCLien, char* idProd,int mes,int quantidade,char tipo);
int height2(PAVL N);
int max2(int a, int b);
PAVL rightRotate2(PAVL y);
PAVL leftRotate2(PAVL x);
int getBalance2(PAVL N);
int procura2(PAVL node, char* id);
PAVL insert2(PAVL node, char* id,int mes,int quantidade,char tipo);
void getComprasClient(KAVL node,char client[],int array[],int size);
void totComprasTree(PAVL node,int array[],int size);
char* strdup(char*);
void getNbyOrder(KAVL node,char client[],int mes,char** arrayProd,int* arrayTot,int size);
void putByOrder(PAVL node,int mes,char** arrayProd,int* arrayTot,int size);
void putInPlace(int totComprados,char** arrayProd,int* arrayTot,int size);
void swapInt(int* arrayTot,int origem,int destino);
void swapChar(char** arrayProd,int origem,int destino);
void init_array_aux (int array_aux[]);
int verifica_array(int array_aux []);
int correr_produtos(PAVL node, char** array, int size, int array_aux[]);
int get_clients_buy_every_month(KAVL node,char** array,int size);
void calc_tot_clientes(KAVL node, int array[]);
void meterarray(int array_aux[], int array[]);
void calc_tot_clientes_aux(PAVL node, int array_aux[]);
KAVL procura_avlCompras2(KAVL node, char* id);
void top3_client(KAVL node, char* cliente, int array_valores[], char* array_codigos[]);
void top3_product(KAVL node, int array_valores[], char* array_codigos[]);
int tot_didnt_buy(KAVL node, int size);
int compra_n(KAVL node, char** array, char* produto, int size);
int compra_p(KAVL node, char** array, char* produto, int size);
char** init_array_clientes_n(KAVL node[],int maxSize,int* size,char* produto);
char** init_array_clientes_p(KAVL node[],int maxSize,int* size,char* produto);
char** init_array_ord(KAVL node,char client[],int mes,int* arrayTot,int maxSize);
char** init_buy_every_month(KAVL node[],int maxSize,int* size);


#endif