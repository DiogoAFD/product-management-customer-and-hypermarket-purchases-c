#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "avlContabilidade.h"


typedef struct A{
    int tot_vendas;
    float facturacao; /* total e facturaçao mensal*/
}A;


struct TAVL{ 
	char* id; /*identificador vai  ser o produto*/
    A normal[12];
    A promocao[12];
	struct TAVL *left;
	struct TAVL *right;
	int height; /*Height height*/
    int totN, totP; /* total e facturaçao dos dois tipos*/
    float fatN, fatP;
}TAVL;


void init_array_normal ( A normal[]){
    int i;
    for (i = 0; i < 12; i++){
        normal[i].tot_vendas= 0;
        normal[i].facturacao= 0;
    }
}


void init_array_promocao ( A promocao[]){
    int i;
    for (i = 0; i < 12; i++){
        promocao[i].tot_vendas= 0;
        promocao[i].facturacao= 0;
    }
}


int height_contabilidade(COAVL N){
	if (N == NULL) return 0;
	return N-> height;
}


int max_contabilidade(int a, int b){
	return (a > b) ? a : b;
}


COAVL newNode_contabilidade (char * id){
	COAVL node = (COAVL)malloc(sizeof(TAVL));
	node->id=strdup(id);
    init_array_normal(node->normal);
    init_array_promocao(node->promocao);
    node-> totN =0;
    node-> totP =0;
    node-> fatN =0;
    node-> fatP =0;
    node->left=NULL;
    node->right=NULL;
    node->height=1;
    return node;
}


COAVL rightRotate_contabilidade(COAVL y){
    COAVL x = y->left;
    COAVL z = x->right;
    x->right = y;
    y->left = z;
    y->height = max_contabilidade(height_contabilidade(y->left), height_contabilidade(y->right))+1;
    x->height = max_contabilidade(height_contabilidade(x->left), height_contabilidade(x->right))+1;
    return x;
}

 
COAVL leftRotate_contabilidade(COAVL x){
    COAVL y = x->right;
    COAVL T2 = y->left;
    y->left = x;
    x->right = T2;
    x->height = max_contabilidade(height_contabilidade(x->left), height_contabilidade(x->right))+1;
    y->height = max_contabilidade(height_contabilidade(y->left), height_contabilidade(y->right))+1;
    return y;
}


int getBalance_contabilidade(COAVL N){ /* retorna o balanceamento*/
    if (N == NULL)
        return 0;
    return height_contabilidade(N->left) - height_contabilidade(N->right);
}


int procura_contabilidade(COAVL node, char* id){
	if(node == NULL) return 0;
	if (strcmp(id,node->id) == 0) return 1;
    else if (strcmp(id,node->id) < 0) return procura_contabilidade(node->left, id);
    else return procura_contabilidade(node->right, id);
}


COAVL insert_contabilidade2(COAVL node, char* id,float preco,int quantidade,int mes,char tipo){
    int balance = 0;
    if (node == NULL)
        return(newNode_contabilidade(id));
    if (strcmp(id,node->id) != 0){
        if (strcmp(id,node->id) < 0) node->left  = insert_contabilidade2(node->left, id,preco,quantidade,mes,tipo);
        else node->right = insert_contabilidade2(node->right, id,preco,quantidade,mes,tipo);
        node->height = max_contabilidade(height_contabilidade(node->left), height_contabilidade(node->right)) + 1;
        balance = getBalance_contabilidade(node);
        if (balance > 1 && strcmp(id,node->left->id) < 0) return rightRotate_contabilidade(node);
        if (balance < -1 &&  strcmp(id,node->right->id) >= 0) return leftRotate_contabilidade(node);
        if (balance > 1 &&  strcmp(id,node->left->id) >= 0){
            node->left =  leftRotate_contabilidade(node->left);
            return rightRotate_contabilidade(node);
        }
        if (balance < -1 &&  strcmp(id,node->right->id) < 0){
            node->right = rightRotate_contabilidade(node->right);
            return leftRotate_contabilidade(node);
        }
        return node;
    }
    else {
        if(tipo=='N'){
            node->totN+=1;
            node->fatN+=preco*quantidade;
            node->normal[mes-1].tot_vendas++;
            node->normal[mes-1].facturacao+=preco*quantidade;
        }
        else{
            node->totP+=1;
            node->fatP+=preco*quantidade;
            node->promocao[mes-1].tot_vendas++;
            node->promocao[mes-1].facturacao+=preco*quantidade;
        }
        return node;
    }
}

COAVL procura_contabilidadeN(COAVL node, char*id){
        if(node == NULL) return NULL;
        if (strcmp(id,node->id) == 0) return node;
        else if (strcmp(id,node->id) < 0) return procura_contabilidadeN(node->left, id);
        else return procura_contabilidadeN(node->right, id);
}


/*Querie 3 ######################################################################################################*/

void contab_compras(COAVL node, char * cod_produto, int i, float array[]){
    int tot_comprasN=0;
    int tot_comprasP=0;
    float tot_fact=0;
    COAVL node2=NULL;
    if( procura_contabilidadeN(node, cod_produto) != NULL){
        node2 = procura_contabilidadeN(node, cod_produto);
        tot_comprasN = node2->normal[i].tot_vendas;
        tot_comprasP = node2->promocao[i].tot_vendas;
        tot_fact     = (node2->normal[i].facturacao) + (node2->promocao[i].facturacao);
        array[0] = tot_comprasN;
        array[1] = tot_comprasP;
        array[2] = tot_fact;
    }else{
        array[0] = -1;
        array[1] = -1;
        array[2] = -1;
    }
}
/*####################################################################################################################*/

/*Querie 4 ##############################################################################################################*/
 
int foiVendido(COAVL node,char** array,int size){
    if (node != NULL){
        size=foiVendido(node->left,array,size);
        if(node->totN == 0 && node->totP == 0 ){
            (array[size])=(char*)strdup(node->id);
            size++;
        }
        size=foiVendido(node->right,array,size);
    }
    return size;
}



char** initArray(COAVL node[],int maxSize,int* size){
    int i;
    char** array=NULL;
    int aux=0;
    if(node!=NULL){
        array=(char**) malloc(maxSize*sizeof(char*));
        array[0]=NULL;
        for (i=0;i<26;i++){
            aux=foiVendido(node[i],array,aux);
        }
    }
    *size=aux;
    return array;
}

/*#########################################################################################################################*/

/*Querie 7 ###########################################################################################################*/

int totCompras_XtoY(COAVL node, int mes_inicial, int mes_final){
  int j;
  int y=0;
  if (node != NULL){    
        for (j= mes_inicial-1; j < mes_final; j++){
            y += (node->normal[j].tot_vendas) + (node->promocao[j].tot_vendas);
        }
        y+=totCompras_XtoY(node->left, mes_inicial, mes_final);
        y+=totCompras_XtoY(node->right, mes_inicial, mes_final);
    }
return y;
}



float totFacturacao_XtoY(COAVL node, int mes_inicial, int mes_final){
    int j;
    float y=0;
    if (node != NULL){    
        for (j= mes_inicial-1; j < mes_final; j++){
            y += (node->normal[j].facturacao) + (node->promocao[j].facturacao);  
        }
        y+=totFacturacao_XtoY(node->left, mes_inicial, mes_final);
        y+=totFacturacao_XtoY(node->right, mes_inicial, mes_final);
    }
    return y;
}

/*############################################################################################################*/

/*QUERIE 11 ###################################################################################################*/

void calc_tot_compras(COAVL node, int array[]){
    int i;
    if (node !=NULL){
        calc_tot_compras(node->left, array);
        for (i=0; i<12; i++){
            array[i] += (node->normal[i].tot_vendas) + (node->promocao[i].tot_vendas);
        }
        calc_tot_compras(node->right, array);
    }
}

/*##################################################################################################################*/



