#include <stdio.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "avl_modulos.h"
#include "avlContabilidade.h"
#include "avlCompras.h"	
#include "gestHiper.h"






#define MAXSTRINGSIZE 64

GAVL avl_clientes[26], avl_produtos[26];
KAVL avl_compras[26];		/*vou meter por ordem alfabetico do codigo de cliente */
COAVL avl_contabilidade[26]; 	/*vou meter por ordem alfabetico do codigo de produto */
int totClientes,totProdutos;



int main(int argc, char** argv){
	int totCompras,totComprasVal;
	if(argc>1){
		if(argc!=4){
			printf("Número errado de argumentos\n");
		}
		totClientes=leituraClientes(argv[1]);
		totProdutos=leituraProdutos(argv[2]);
		totCompras=leituraCompras(argv[3],&totComprasVal);
		printf("Ficheiro lido para Clientes: %s Linhas Totais: %d Linhas Válidas: %d \n",argv[1],totClientes,totClientes);
		printf("Ficheiro lido para Produtos: %s Linhas Totais: %d Linhas Válidas: %d \n",argv[2],totProdutos,totProdutos);
		printf("Ficheiro lido para Produtos: %s Linhas Totais: %d Linhas Válidas: %d \n",argv[3],totCompras,totComprasVal);

	}
	else{
		
		totClientes=leituraClientes("FichClientes.txt");
		totProdutos=leituraProdutos("FichProdutos.txt");
		totCompras=leituraCompras("Compras.txt",&totComprasVal);
		printf("Ficheiro lido para Clientes: FichClientes.txt Linhas Totais: %d Linhas Válidas: %d \n",totClientes,totClientes);
		printf("Ficheiro lido para Produtos: FichProdutos.txt Linhas Totais: %d Linhas Válidas: %d \n",totProdutos,totProdutos);
		printf("Ficheiro lido para Produtos: Compras.txt Linhas Totais: %d Linhas Válidas: %d \n",totCompras,totComprasVal);
	}
	apresentaMenu();
	return 0;
}

int leituraClientes(char* filename){
	FILE* clientes;
	char s2[MAXSTRINGSIZE];
	char *s1;
	int counter=0;
	clientes=fopen(filename,"r");
	if(clientes==NULL){
		printf("Impossível abrir ficheiro de clientes.\n");
		exit(0);
	}
	while(fgets(s2, MAXSTRINGSIZE ,clientes)!= NULL){
		s1= strtok(s2, "\r\n");
		avl_clientes[*s1-'A']=insert(avl_clientes[*s1-'A'],s1);
		avl_compras[*s1-'A']=insert_avlCompras(avl_compras[*s1-'A'],s1,NULL,0,0,'A');
		counter++;
	}
	fclose(clientes);
	return counter;
}


int leituraProdutos(char* filename){
	FILE* produtos;
	char s2[MAXSTRINGSIZE];
	char *s1;
	int counter=0;
	produtos=fopen(filename,"r");
	if(produtos==NULL){
		printf("Impossível abrir ficheiro de produtos.\n");
		exit(0);
	}
	while(fgets(s2, MAXSTRINGSIZE ,produtos)!= NULL){
		s1= strtok(s2, "\r\n");
		avl_produtos[*s1-'A']=insert(avl_produtos[*s1-'A'],s1);
		avl_contabilidade[*s1-'A']=insert_contabilidade2(avl_contabilidade[*s1-'A'],s1,0,0,0,'A');
		counter++;
	}
	fclose(produtos);
	return counter;
}


int leituraCompras(char* filename,int* validas){
	FILE *compras;
	char* token;
	int aux=0;
	int total=0;
	int totVal=0;
	/*Compra com;*/
	char s2[MAXSTRINGSIZE];
	char produto[MAXSTRINGSIZE];
	float preco=0;
	int unidades=0;
	char tipo='A';
	char cliente[MAXSTRINGSIZE];
	int mes=0;
	compras=fopen(filename,"r");
	if(compras==NULL){
		printf("Impossível abrir ficheiro de compras.\n");
		exit(0);
	}
	while(fgets(s2, MAXSTRINGSIZE ,compras)!= NULL){
		s2[(strlen(s2))-1] = '\0';
		token=strtok(s2," " );
		strcpy(produto,token);
		aux=1;
		while(token!=NULL){
			token=strtok(NULL," ");
			if(token != NULL){
				switch(aux){
					case 1:{preco=atof(token); aux++;}break;

					case 2:{unidades=atoi(token);aux++;}break;

					case 3:{tipo=token[0];aux++;}break;

					case 4:{strcpy(cliente,token);aux++;}break;

					case 5:{mes=atoi(token);aux++;}break;
				}
			}
		}
		if(testeValidade(produto,cliente,mes,unidades)){
			totVal++;
			avl_compras[*cliente-'A']=insert_avlCompras(avl_compras[*cliente-'A'],cliente,produto,mes,unidades,tipo);
			avl_contabilidade[*produto-'A']=insert_contabilidade2(avl_contabilidade[*produto-'A'],produto,preco,unidades,mes,tipo);
		}
		total++;
	}
	fclose(compras);
	*validas=totVal;
	return total;
}


int testeValidade(char* produto,char* cliente, int mes,int quantidade){
	if(mes<=0 || mes>12){
		return 0;
	}
	if(quantidade<=0){
		return 0;
	}
	if(!procura(avl_produtos[*produto-'A'],produto)){
		return 0;
	}
	if(!procura(avl_clientes[*cliente-'A'],cliente)){
		return 0;
	}
	return 1;
}


void querie2(){
	char fLetter,aux;
	int size=0;
	char** array=NULL;
	printf("Escreva uma letra maiúscula por favor \n");
	aux=scanf(" %c", &fLetter);
	fLetter=toupper(fLetter);
	array=getElements(avl_produtos[fLetter-'A'],totProdutos,&size);
	printArrayStrings(array,size);
	(void) aux;
}


void querie3(){
 	int aux1,aux2,mes;
 	char cod_produto[7];
 	float array[3];
 	printf("Escreva o mês por favor \n");
 	aux1=scanf(" %d", &mes);
 	printf("Escreva o código de produto por favor EX: AA1111 \n");
 	aux2=scanf(" %s", cod_produto);
	contab_compras(avl_contabilidade[cod_produto[0]-'A'], cod_produto, mes-1, array);
	if(array[0]<0){
		printf("Produto inexistente\n");
		apresentaMenu();
	}else{
		printf("Total de compras normais do produto %s no mes %d -> %f\n", cod_produto, mes, array[0]);
		printf("Total de compras em promoção do produto %s no mes %d -> %f\n",cod_produto, mes, array[1]);
		printf("Facturação no mes %d com a venda do produto %s -> %f\n", mes, cod_produto, array[2]);
	}
 	(void) aux1;
	(void) aux2;
	apresentaMenu();
}

void querie4(){
	int total=0;
	char** array;
	array=initArray(avl_contabilidade,totProdutos,&total);
	printArrayStrings(array,total);
}


void querie5(){
	FILE *f;
	int aux=0,aux2=0;
	char opcao;
	int opcaoVal=0;
	int totais[12];
	char client[6];
	printf("Escreve um código de cliente por favor. Ex: AA111\n");
	aux=scanf(" %s", client);
	getComprasClient(avl_compras[client[0]-'A'],client,totais,12);
	if(totais[0]<0){
		printf("Cliente inexistente\n");
		apresentaMenu();
	}else{
		printf("________________________________\n");
		printf("|Mês          |    Total        |\n");
		printf("________________________________\n");
		for(aux=0;aux<12;aux++){
			printf("|%d             |    %d          |\n",aux+1,totais[aux]);
			printf("________________________________\n");
		}
		printf("Pressione 'Q' para sair.\n");
		printf("Pressione 'G' para gravar em ficheiro.\n");
		aux2=scanf(" %c", &opcao);
		opcao=toupper(opcao);
		while(!opcaoVal){
					switch(opcao){
						case 'G':{	f=fopen("Tabela.txt","w+");
									fprintf(f,"________________________________\n");
									fprintf(f,"|Mês          |    Total        |\n");
									fprintf(f,"________________________________\n");
									for(aux=0;aux<12;aux++){
									fprintf(f,"|%d             |    %d          |\n",aux+1,totais[aux]);
									fprintf(f,"________________________________\n");
								}
									fclose(f);
									opcaoVal=1;}break;
	
						case 'Q':{opcaoVal=1;} break;
	
						default:{printf("Insira uma opção válida\n"); aux2=scanf(" %c", &opcao);opcao=toupper(opcao);} 
					}
				}
		}
	(void) aux2;	
	apresentaMenu();
}


void querie6(){
	char fLetter,aux;
	int size=0;
	char** array=NULL;
	printf("Escreva uma letra por favor \n");
	aux=scanf(" %c", &fLetter);
	fLetter=toupper(fLetter);
	array=getElements(avl_clientes[fLetter-'A'],totClientes,&size);
	printArrayStrings(array,size);
	(void) aux;
}


void querie7(){
	int mes_inicial, mes_final;
	int aux1, aux2;
	int x=0;
	float y=0;
	int i;
	printf("Escreva o mês inicial por favor\n");
	aux1=scanf(" %d", &mes_inicial);
	printf("Escreva o mês final por favor\n");
	aux2=scanf(" %d", &mes_final);
	for (i=0;i<26;i++){
 		x += totCompras_XtoY(avl_contabilidade[i], mes_inicial, mes_final);
 		y += totFacturacao_XtoY(avl_contabilidade[i], mes_inicial, mes_final);
	}
	printf("Total de compras entre o mês %d e o mês %d -> %d\n", mes_inicial, mes_final, x);
	printf("Facturação total entre o mês %d e o mês %d -> %f\n", mes_inicial, mes_final, y);
	(void) aux1;
	(void) aux2;
	apresentaMenu();
}


void querie8(){
	int aux;
	int size=0;
	int size2=0;
	char** array_clientes_normal=NULL;
	char** array_clientes_promocao=NULL;
	char produto[7];
	printf("Escreva o código de produto por favor EX:AA1111 \n");
 	aux=scanf(" %s", produto);
	array_clientes_normal=init_array_clientes_n(avl_compras,totProdutos,&size,produto);
	array_clientes_promocao=init_array_clientes_p(avl_compras,totProdutos,&size2,produto);
 	printf("Clientes que compraram o produto %s no tipo normal\n", produto);
 	printArrayStrings_notToMenu(array_clientes_normal,size);
 	printf("Clientes que compraram o produto %s no tipo promoção\n", produto);
 	printArrayStrings(array_clientes_promocao,size2);
 	(void) aux;
}


void querie9(){
	int aux,mes,n;
 	char cliente[6];
 	char** arrayProd=NULL;
 	int* arrayTot=NULL;
	printf("Escreva o mês por favor \n");
 	aux=scanf(" %d", &mes);
 	printf("Escreva o código de cliente por favor EX:AA111 \n");
 	aux=scanf(" %s", cliente);
 	printf("Escreva o número de produtos que deseja ordenar\n");
 	aux=scanf(" %d", &n);
	arrayProd=init_array_ord(avl_compras[cliente[0]-'A'],cliente,mes,arrayTot,n);
	aux=0;
	if(arrayProd!=NULL){
		printf("Os produtos mais comprados por o cliente %s no mes %d sao:\n",cliente,mes);
		while(aux<n && arrayProd[aux]!=NULL){
			printf("%s\n",arrayProd[aux]);
			aux++;	
		}
	}
	apresentaMenu();
}

void querie10(){
	char** array=NULL;
	int size=0;
	array=init_buy_every_month(avl_compras,totClientes,&size);
	printArrayStrings(array, size);
}


void querie11(){
	int i;
	int array_totcompras[12];
	int array_totclientes[12];
	FILE *f;
	for (i=0; i<12; i++){
		array_totclientes[i]=0;
		array_totcompras[i]=0;
	}
	for (i=0; i<26; i++){
		calc_tot_compras(avl_contabilidade[i], array_totcompras);
		calc_tot_clientes(avl_compras[i], array_totclientes);
	}
	f = fopen("excel.csv", "w+");
	fprintf(f,"\"Mês\",\"#Compras\",\"#Clientes\"\n");
	for (i=0; i<12; i++){
		fprintf(f,"\"%d\",\"%d\",\"%d\"\n", i+1, array_totcompras[i], array_totclientes[i]);
	}
	printf("Foi criado um ficheiro CSV.\n");
	fclose(f);
	apresentaMenu();
}



void querie13(){
	int aux,i;
	char cliente[6];
	char* array_codigos[3];
 	int   array_valores[3];
 	for (i=0; i<3; i++){
 		array_codigos[i]=NULL;
 		array_valores[i]=0;
 	}
	printf("Escreva o código de cliente por favor EX:AA111.  \n");
 	aux=scanf(" %s", cliente);
 	top3_client(avl_compras[cliente[0]-'A'], cliente, array_valores, array_codigos);
 	i=0;
 	if(array_codigos[0]!=NULL){
 		printf ("Produtos mais comprados pelo cliente %s:\n", cliente);
 		while(i<3){
 			if (array_codigos[i] != NULL){
 				printf ("-> comprou %d produtos %s \n", array_valores[i], array_codigos[i]);
 				i++;
			}
			else {break;}
		}
	}
 	(void) aux;
 	apresentaMenu();
}


void querie14(){
	int size=0;
	int size2=0;
	int i;
	for (i=0; i<26; i++){ 
		size = tot_didnt_buy(avl_compras[i],size);
	}
	initArray(avl_contabilidade,totProdutos,&size2);
	printf("Houve %d produtos não comprados\n",size2);
	printf("Houve %d clientes que não realizaram nenhuma compra.\n",size);
	apresentaMenu();
}


void printArrayStrings(char** array,int size){
	int aux=0,aux2=0,parar=0,pag=0,pagAtual=1;
	char opcao;
	int opcaoVal=0,i=0;
	printf("Total de resultados: %d\n",size);
	if(size%15!=0){
		pag=(size/15)+1;
	}else{
		pag=size/15;
	}
	printf("Total de páginas: %d\n",pag);
	if(array!=NULL){
		while(!parar && i<=size && array[i]!=NULL ){
			if(aux==0){
				printf("A mostrar página %d/%d\n",pagAtual,pag);
			}
			printf("%s\n",array[i]);
			i++;
			aux++;
			if(aux==15){
				opcaoVal=0;
				printf("\n\n");
				printf("Próximos 15 pressione N\n");
				printf("Anteriores 15 pressione A\n");
				printf("Sair pressione Q\n");
				aux2=scanf(" %c", &opcao);
				opcao=toupper(opcao);
				aux=0;
				while(!opcaoVal){
					switch(opcao){
						case 'Q':{	parar=1;
									opcaoVal=1;}break;

						case 'N':{opcaoVal=1;pagAtual++;} break;

						case 'A':{	i=i-30;
									pagAtual--;
									if(pagAtual<1){
										pagAtual=1;
									}
									if(i<0){
										i=0;
									}
									opcaoVal=1;
								};break;
		
						default:{printf("Insira uma opção válida\n"); aux2=scanf(" %c", &opcao);opcao=toupper(opcao);} 
					}
				}	
			}
		}
	}
	(void) aux2;
	apresentaMenu(totClientes,totProdutos);
}

void printArrayStrings_notToMenu(char** array,int size){
	int aux=0,aux2=0,parar=0,pag=0,pagAtual=1;
	char opcao;
	int opcaoVal=0,i=0;
	printf("Total de resultados: %d\n",size);
	if(size%15!=0){
		pag=(size/15)+1;
	}else{
		pag=size/15;
	}
	printf("Total de páginas: %d\n",pag);
	if(array!=NULL){
		while(!parar && i<=size && array[i]!=NULL){
			if(aux==0){
				printf("A mostrar página %d/%d\n",pagAtual,pag);
			}
			printf("%s\n",array[i]);
			i++;
			aux++;
			if(aux==15){
				opcaoVal=0;
				printf("\n\n");
				printf("Próximos 15 pressione N\n");
				printf("Anteriores 15 pressione A\n");
				printf("Sair pressione Q\n");
				aux2=scanf(" %c", &opcao);
				opcao=toupper(opcao);
				aux=0;
				while(!opcaoVal){
					switch(opcao){
						case 'Q':{	parar=1;
									opcaoVal=1;}break;

						case 'N':{opcaoVal=1;pagAtual++;} break;

						case 'A':{	i=i-30;
									if(i<0){
										i=0;
									}
									opcaoVal=1;
									pagAtual--;
									if(pagAtual<1){
										pagAtual=1;
									}
								};break;
		
						default:{printf("Insira uma opção válida\n"); aux2=scanf(" %c", &opcao);opcao=toupper(opcao);} 
					}
				}	
			}
		}
	}
	(void) aux2;
	
}

void apresentaMenu(){
	int opcao,aux;
	int opcaoVal=0;
	printf("\n\n");
	printf("Bem-Vindo ao GestHiper\n\n");
	printf("2-Obter a lista de produtos iniciados por uma letra.\n");
	printf("3-Obter o total de compras num mês de um determinado produto distinguido por N e P.\n");
	printf("4-Obter a lista de produtos que ninguém comprou.\n");
	printf("5-Obter tabela com o número total de produtos comprados de um cliente, separados por mês.\n");
	printf("6-Obter a lista de clientes iniciados por uma letra.\n");
	printf("7-Obter o total de compras e facturação num intervalo de meses.\n");
	printf("8-Obter os clientes que compraram um produto distinguido entre N e P.\n");
	printf("9-Obter os n produtos mais comprados de um cliente num mês.\n");
	printf("10-Obter os clientes que fizeram compras todos os meses do ano.\n");
	printf("11-Obter um ficheiro CSV com o total de compras e clientes por mês.\n");
	printf("13-Determinar os 3 produtos mais comprados por um cliente no ano.\n");
	printf("14-Determinar o número de clientes que não realizaram compras e o número de produtos que ninguem comprou.\n");
	printf("15-Sair\n");
	printf("Escolha uma opção:\n");
	aux=scanf("%d", &opcao);
	while(!opcaoVal){
		switch(opcao){
			case 15:{opcaoVal=1;}break;

			case 2:{printf("\n\n");querie2();opcaoVal=1;} break;

			case 3:{printf("\n\n");querie3();opcaoVal=1;};break;

			case 4:{printf("\n\n");querie4();opcaoVal=1;} break;

			case 5:{printf("\n\n");querie5();opcaoVal=1;} break;

			case 6:{printf("\n\n");querie6();opcaoVal=1;} break;

			case 7:{printf("\n\n");querie7();opcaoVal=1;} break;

			case 8:{printf("\n\n");querie8();opcaoVal=1;} break;

			case 9:{printf("\n\n");querie9();opcaoVal=1;} break;

			case 10:{printf("\n\n");querie10();opcaoVal=1;} break;

			case 11:{printf("\n\n");querie11();opcaoVal=1;} break;

			case 13:{printf("\n\n");querie13();opcaoVal=1;} break;

			case 14:{printf("\n\n");querie14();opcaoVal=1;} break;
		
			default:{printf("Insira uma opção válida\n"); aux=scanf("%d", &opcao);} 
		}
	}
	(void) aux;	
}
	