#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "avlCompras.h"




struct BAVL{
    char* id; /*codigo do Produto*/
    int normal[12];
    int promocao[12];
    struct BAVL *left;
    struct BAVL *right;
    int height;
}BAVL;


struct CAVL{ 
	char* id;  /*identificador vai  ser o cliente*/
    PAVL produtList[26]; 
	struct CAVL *left;
	struct CAVL *right;
	int height; /*Height height*/
}CAVL;


void init_array_normal_avlCompras ( int normal[]){
    int i;
    for (i = 0; i < 12; i++){
        normal[i]= 0;
    }
}


void init_array_promocao_avlCompras ( int promocao[]){
    int i;
    for (i = 0; i < 12; i++){
        promocao[i]= 0;
    }
}


PAVL init_produtList (char* idProd,int mes,int quantidade,char tipo){
       PAVL node= (PAVL)malloc(sizeof(BAVL)) ; 
       node->id= strdup(idProd);  /*codigo produto*/
       init_array_normal_avlCompras(node->normal);
       init_array_promocao_avlCompras(node->promocao);
       if(tipo=='N'){
            node->normal[mes-1]+=quantidade;
       }else{
            node->promocao[mes-1]+=quantidade;
       }
       node->right=NULL;
       node->left=NULL;
       node->height=1;
       return node; 
}



int height_avlCompras(KAVL N){
	if (N == NULL) return 0;
	return N-> height;
}


int max_avlCompras(int a, int b){
	return (a > b) ? a : b;
}

KAVL newNode_avlCompras (char* idCLien, char* idProd){
     int i;
	KAVL node = (KAVL)malloc(sizeof(CAVL));
	node->id=strdup(idCLien);
    for (i=0; i<26; i++){
        node->produtList[i]= NULL;
    }
    node->left=NULL;
	node->right=NULL;
	node->height=1;
    return node;
}


KAVL rightRotate_avlCompras(KAVL y){
    KAVL x = y->left;
    KAVL z = x->right;
    x->right = y;
    y->left = z;
    y->height = max_avlCompras(height_avlCompras(y->left), height_avlCompras(y->right))+1;
    x->height = max_avlCompras(height_avlCompras(x->left), height_avlCompras(x->right))+1;
    return x;
}
 
KAVL leftRotate_avlCompras(KAVL x){
    KAVL y = x->right;
    KAVL T2 = y->left;
    y->left = x;
    x->right = T2;
    x->height = max_avlCompras(height_avlCompras(x->left), height_avlCompras(x->right))+1;
    y->height = max_avlCompras(height_avlCompras(y->left), height_avlCompras(y->right))+1;
    return y;
}

int getBalance_avlCompras(KAVL N){
    if (N == NULL)
        return 0;
    return height_avlCompras(N->left) - height_avlCompras(N->right);
}


int procura_avlCompras(KAVL node, char* id){
	if(node == NULL) return 0;
	if (strcmp(id,node->id) == 0) return 1;
    else if (strcmp(id,node->id) < 0) return procura_avlCompras(node->left, id);
    else return procura_avlCompras(node->right, id);
}

KAVL insert_avlCompras(KAVL node, char* idCLien, char* idProd,int mes,int quantidade,char tipo){
	int balance = 0;
	if (node == NULL)
        return(newNode_avlCompras(idCLien,idProd));
	if (strcmp(idCLien,node->id) != 0){
		if (strcmp(idCLien,node->id) < 0) node->left  = insert_avlCompras(node->left, idCLien, idProd,mes,quantidade,tipo);
        else node->right = insert_avlCompras(node->right, idCLien, idProd,mes,quantidade,tipo);
    	node->height = max_avlCompras(height_avlCompras(node->left), height_avlCompras(node->right)) + 1;
		balance = getBalance_avlCompras(node);
		if (balance > 1 && strcmp(idCLien,node->left->id) < 0) return rightRotate_avlCompras(node);
		if (balance < -1 &&  strcmp(idCLien,node->right->id) >= 0) return leftRotate_avlCompras(node);
        if (balance > 1 &&  strcmp(idCLien,node->left->id) >= 0){
            node->left =  leftRotate_avlCompras(node->left);
            return rightRotate_avlCompras(node);
        }
        if (balance < -1 &&  strcmp(idCLien,node->right->id) < 0){
            node->right = rightRotate_avlCompras(node->right);
            return leftRotate_avlCompras(node);
        }
     	return node;
    }
    else { node->produtList[*idProd-'A']=insert2(node->produtList[*idProd-'A'],idProd,mes,quantidade,tipo);
            return node;
        }
}


int height2(PAVL N){
    if (N == NULL) return 0;
    return N-> height;
}


int max2(int a, int b){
    return (a > b) ? a : b;
}


PAVL rightRotate2(PAVL y){
    PAVL x = y->left;
    PAVL z = x->right;
    x->right = y;
    y->left = z;
    y->height = max2(height2(y->left), height2(y->right))+1;
    x->height = max2(height2(x->left), height2(x->right))+1;
    return x;
}
 
PAVL leftRotate2(PAVL x){
    PAVL y = x->right;
    PAVL T2 = y->left;
    y->left = x;
    x->right = T2;
    x->height = max2(height2(x->left), height2(x->right))+1;
    y->height = max2(height2(y->left), height2(y->right))+1;
    return y;
}


int getBalance2(PAVL N){ 
    if (N == NULL)
        return 0;
    return height2(N->left) - height2(N->right);
}


int procura2(PAVL node, char* id){
    if(node == NULL) return 0;
    if (strcmp(id,node->id) == 0) return 1;
    else if (strcmp(id,node->id) < 0) return procura2(node->left, id);
    else return procura2(node->right, id);
}



PAVL insert2(PAVL node, char* id,int mes,int quantidade,char tipo){
    int balance = 0;
    if (node == NULL)
        return(node=init_produtList(id,mes,quantidade,tipo));
    if (strcmp(id,node->id) != 0){
        if (strcmp(id,node->id) < 0) node->left  = insert2(node->left, id,mes,quantidade,tipo);
        else node->right = insert2(node->right, id,mes,quantidade,tipo);
        node->height = max2(height2(node->left), height2(node->right)) + 1;
        balance = getBalance2(node);
        if (balance > 1 && strcmp(id,node->left->id) < 0) return rightRotate2(node);
        if (balance < -1 &&  strcmp(id,node->right->id) >= 0) return leftRotate2(node);
        if (balance > 1 &&  strcmp(id,node->left->id) >= 0){
            node->left =  leftRotate2(node->left);
            return rightRotate2(node);
        }
        if (balance < -1 &&  strcmp(id,node->right->id) < 0){
            node->right = rightRotate2(node->right);
            return leftRotate2(node);
        }
        return node;
    }
    else { 
        if(tipo=='N'){
            node->normal[mes-1]+=quantidade;
        }else{
            node->promocao[mes-1]+=quantidade;
        }
        return node;
    }
}


PAVL procura_avlCompras_Produto(PAVL node, char* id){
    if(node == NULL) return NULL;
    if (strcmp(id,node->id) == 0) return node;
    else if (strcmp(id,node->id) < 0) return procura_avlCompras_Produto(node->left, id);
    else return procura_avlCompras_Produto(node->right, id);
}

 
/*Querie 5 ##########################################################################################*/

void getComprasClient(KAVL node,char client[],int array[],int size){
    int i;
    if(node!=NULL){
        if (strcmp(client,node->id) == 0){
            /*Inicializar array*/
            for(i=0;i<size;i++){
                array[i]=0;
            }
            for(i=0;i<26;i++){
                totComprasTree(node->produtList[i],array,size);
            }
        }else if (strcmp(client,node->id) < 0){getComprasClient(node->left,client,array,size);}
        else {getComprasClient(node->right,client,array,size);}
    }else{
        for(i=0;i<size;i++){
                array[i]=-1;
        }
    }
}


void totComprasTree(PAVL node,int array[],int size){
    int i;
    if(node!=NULL){
        for(i=0;i<size;i++){
            array[i]=array[i]+(node->normal[i])+(node->promocao[i]);
        }
        totComprasTree(node->left,array,size);
        totComprasTree(node->right,array,size);
    }
}

/*################################################################################################################################*/

/*QUERIE 8 ##############################################################################################################*/


char** init_array_clientes_n(KAVL node[],int maxSize,int* size,char* produto){
    int i;
    char** array=NULL;
    int aux=0;
    if(node!=NULL){
        array=(char**) malloc(maxSize*sizeof(char*));
        array[0]=NULL;
        for (i=0;i<26;i++){
            aux=compra_n(node[i],array,produto,aux);
        }
    }
    *size=aux;
    return array;
}

char** init_array_clientes_p(KAVL node[],int maxSize,int* size,char* produto){
    int i;
    char** array=NULL;
    int aux=0;
    if(node!=NULL){
        array=(char**) malloc(maxSize*sizeof(char*));
        array[0]=NULL;
        for (i=0;i<26;i++){
            aux=compra_p(node[i],array,produto,aux);
        }
    }
    *size=aux;
    return array;
}


int compra_n(KAVL node, char** array, char* produto, int size){
    int flag=0;
    int i;
    PAVL node2 = NULL;
    if (node != NULL){
        size = compra_n(node->left, array, produto, size);
        node2 = procura_avlCompras_Produto(node->produtList[produto[0]-'A'], produto);
        if (node2 != NULL){
            for (i=0; i<12 && flag != 1; i++){
                if (node2->normal[i] != 0){
                    (array[size])=(char*)strdup(node->id);
                    size++;
                    flag=1;
                }
            }
        }
        size = compra_n(node->right, array, produto, size);
    }
    return size;
}




int compra_p(KAVL node, char** array, char* produto, int size){
    int flag=0;
    int i;
    PAVL node2 = NULL;
    if (node != NULL){
        size = compra_p(node->left, array, produto, size); 
        node2 = procura_avlCompras_Produto(node->produtList[produto[0]-'A'], produto);
        if (node2 != NULL){
            for (i=0; i<12 && flag != 1; i++){
                if (node2->promocao[i] != 0){
                    (array[size])=(char*)strdup(node->id);
                    size++;
                    flag=1;
                }
            }
        }
        size = compra_p(node->right, array, produto, size);
    }
    return size;

}

/*####################################################################################################################*/


/*Querie 9  #######################################################################################################################*/

char** init_array_ord(KAVL node,char client[],int mes,int* arrayTot,int maxSize){
    char** array=NULL;
    if(node!=NULL){
        arrayTot=(int*) malloc(maxSize*sizeof(int));
        array=(char**) malloc(maxSize*sizeof(char*));
        array[0]=NULL;
        getNbyOrder(node,client,mes,array,arrayTot,maxSize);
    }
    return array;
}

void getNbyOrder(KAVL node,char client[],int mes,char** arrayProd,int* arrayTot,int size){
    int i;
    if(node!=NULL){
        if (strcmp(client,node->id) == 0){
            for(i=0;i<size;i++){
                arrayTot[i]=0;
                arrayProd[i]=NULL;
            }
            for(i=0;i<26;i++){
                putByOrder(node->produtList[i],mes,arrayProd,arrayTot,size);
            } 
        }else if (strcmp(client,node->id) < 0){getNbyOrder(node->left,client,mes,arrayProd,arrayTot,size);}
        else {getNbyOrder(node->right,client,mes,arrayProd,arrayTot,size);}
    }
}

void putByOrder(PAVL node,int mes,char** arrayProd,int* arrayTot,int size){
    int totComprados;
    if(node!=NULL){
        totComprados=(node->normal[mes-1])+(node->promocao[mes-1]);
        if(totComprados>arrayTot[size-1]){
            arrayTot[size-1]=totComprados;
            arrayProd[size-1]=strdup(node->id);
            putInPlace(totComprados,arrayProd,arrayTot,size);
        }
        putByOrder(node->left,mes,arrayProd,arrayTot,size);
        putByOrder(node->left,mes,arrayProd,arrayTot,size);
    }
}



/*###############################################################################################################*/


/* Querie 10 ####################################################################################################*/

char** init_buy_every_month(KAVL node[],int maxSize,int* size){
    int i,aux=0;
    char** array=NULL;
    if(node!=NULL){
        array=(char**) malloc(100000*sizeof(char*));
        array[0]=NULL;
        for (i=0; i<26; i++){
            aux=get_clients_buy_every_month(node[i], array, aux);
        }
    }
    *size=aux;
    return array;
}


int correr_produtos(PAVL node, char** array, int size, int array_aux[]){
    int flag=0;
    int i;
    if (node != NULL){
        size=correr_produtos(node->left, array, size, array_aux);
        for (i=0; i<12 && flag != 1 ; i++){
            if (node->normal[i] != 0 || node->promocao[i] != 0){
                array_aux[i]=1;
            }
            if (verifica_array(array_aux)==1){
                (array[size])=(char*)strdup(node->id);
                size++;
                flag=1;                    
            } 
        }
        size=correr_produtos(node->right, array, size, array_aux);
    }
    return size;
}


int get_clients_buy_every_month(KAVL node,char** array,int size){
    int flag=0;
    int j;
    int array_aux[12];
    if (node != NULL){
        init_array_aux(array_aux);
        size=get_clients_buy_every_month(node->left, array, size);
        for(j=0; j<26 && flag != 1; j++){
            if (verifica_array(array_aux)==1){
                (array[size])=(char*)strdup(node->id);
                size++;
                flag=1;             
            }
            correr_produtos(node->produtList[j], array, size, array_aux);   
        }
        size=get_clients_buy_every_month(node->right,array,size);
    }
    return size;
}


/*#########################################################################################################*/

/*Querie 11 ################################################################################################*/

void meterarray(int array_aux[], int array[]){
    int i;
    for(i=0; i<12; i++){
        if (array_aux[i]>0) array[i]++;
    }
}


void calc_tot_clientes_aux(PAVL node, int array_aux[]){
    int flag=0;
    int i;
    if (node != NULL){
        calc_tot_clientes_aux(node->left, array_aux);
        for(i=0; i<12 && flag!=1; i++){
            if (node->normal[i] != 0 || node->promocao[i] != 0){
                array_aux[i]=1;
            }
            if (verifica_array(array_aux)==1){
                flag=1;                    
            } 

        }
        calc_tot_clientes_aux(node->right, array_aux);
    }
} 


void calc_tot_clientes(KAVL node, int array[]){
    int j;
    int flag=0;
    int array_aux[12];
    if (node!=NULL){
        init_array_aux(array_aux);
        calc_tot_clientes(node->left, array);
        for(j=0; j<26 && flag != 1; j++){
            calc_tot_clientes_aux(node->produtList[j], array_aux); 
            if (verifica_array(array_aux)==1){
                flag=1;
            }
        }
        meterarray(array_aux, array);
        calc_tot_clientes(node->right, array);
    }
}


/*####################################################################################################################*/


/*QUERIE 13 ############################################################################################################*/

KAVL procura_avlCompras2(KAVL node, char* id){
    if(node == NULL) return NULL;
    if (strcmp(id,node->id) == 0) return node;
    else if (strcmp(id,node->id) < 0) return procura_avlCompras2(node->left, id);
    else return procura_avlCompras2(node->right, id);
}

void top3_product(KAVL node, int array_valores[], char* array_codigos[]){
    int i,j;
    int tot=0;
     if (node != NULL){
        for (i=0; i<26; i++){
            if (node->produtList[i] != NULL){
                tot=0;
                for(j=0; j<12; j++){
                    tot += (node->produtList[i]->normal[j]) + (node->produtList[i]->promocao[j]);
                }
                if (tot > array_valores[2]){
                    array_codigos[2]=strdup(node->produtList[i]->id);
                    array_valores[2]=tot;
                    putInPlace (tot, array_codigos, array_valores, 3);
                }

            top3_product(node->left, array_valores, array_codigos);
            top3_product(node->right, array_valores, array_codigos);
            }
        }
    }
}



void top3_client(KAVL node, char* cliente, int array_valores[], char* array_codigos[]){
    KAVL node2;
    node2= procura_avlCompras2(node, cliente);
    if (node2 != NULL){
        top3_product(node2, array_valores, array_codigos);
    }
}

/*####################################################################################################################*/


/*QUERIE 14 #######################################################################################################*/

int tot_didnt_buy(KAVL node, int size){
    int flag=0;
    int i;
    if (node != NULL){
         size = tot_didnt_buy(node->left,size);
        for (i=0; i<26 && flag!=1 ;i++){ /*CORRER PRODUTOS*/
            if (node->produtList[i] != NULL){
                flag = 1;
            }
        }
        if (flag == 0) {
            size++;
        }
        size = tot_didnt_buy(node->right,size);
    }
    return size;
}
/*##################################################################################################################*/

/*AUX #######################################################################################################*/

void init_array_aux (int array_aux[]){
    int i;
    for(i=0; i<12; i++)
        array_aux[i]=0;
}

int verifica_array(int array_aux []){
    int i=0;
    while(i<12){
        if (array_aux[i]!=0) i++;
        else return 0;
    }
    return 1;
}

void putInPlace(int totComprados,char** arrayProd,int* arrayTot,int size){
    
    int aux=size-1;
    while(aux>0){
        if(totComprados>arrayTot[aux-1]){
            swapInt(arrayTot,aux,aux-1);
            swapChar(arrayProd,aux,aux-1);
            aux--;
        }
        else{
            aux=0;
        }
    }   
}

void swapInt(int* arrayTot,int origem,int destino){
    
    int aux;
    aux=arrayTot[origem];
    arrayTot[origem]=arrayTot[destino];
    arrayTot[destino]=aux;
}

void swapChar(char** arrayProd,int origem,int destino){
    char* aux;
    if(arrayProd[destino]!=NULL){
        aux=strdup(arrayProd[origem]);
        arrayProd[origem]=strdup(arrayProd[destino]);
        arrayProd[destino]=strdup(aux);
    }else{
        arrayProd[destino]=strdup(arrayProd[origem]);
        arrayProd[origem]=NULL;
    }
}
/*###################################################################################################################*/






