# **GESTHIPER** #
## ** Product Management, Customer and Hypermarket Purchases** ##

The project is an application called GESTHIPER, developed in C language to create, manage and consulting a shopping catalog with a large volume of data. This is a large-scale project, where the amount of data to be processed is high. This project was developed taking into account the concepts of modularity and encapsulation.