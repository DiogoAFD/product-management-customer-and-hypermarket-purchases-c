
CFLAGS=-Wall -ansi -pedantic -O2

all: gestHiper.o avl_modulos.o avlContabilidade.o avlCompras.o
	gcc $(CFLAGS) -o gestHiper gestHiper.o avl_modulos.o avlContabilidade.o avlCompras.o
	
clean:
	rm -f *.o

